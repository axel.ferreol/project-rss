# Use an official Node.js LTS (Long Term Support) as a parent image
FROM node:21.7.1

# Set the working directory in the container
WORKDIR /app/react-app

# Copy package.json and yarn.lock (or package-lock.json if using npm)
COPY react-app/package.json react-app/package-lock.json /app/react-app/

# Install dependencies
RUN npm install

# Copy the rest of the application
COPY react-app/ /app/react-app

# Expose port 9000 to the outside world (assuming your development server runs on port 9000)
EXPOSE 9000

# Start the development server when the container launches
CMD ["npm", "run", "dev"]