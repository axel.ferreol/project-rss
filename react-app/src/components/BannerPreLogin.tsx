import { useNavigate } from "react-router-dom";

function BannerPreLogin() {
  let navigate = useNavigate();
  const handleHeaderClick = () => {
    navigate("/");
  };
  return (
    <div className="container">
      <h1
        className="display-4 text-center font-weight-bold text-success"
        onClick={handleHeaderClick}
      >
        RSS aggregator
      </h1>
    </div>
  );
}

export default BannerPreLogin;
