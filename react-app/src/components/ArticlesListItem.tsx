import { Article, ArticlesList } from "../types/types";
import {
  get_latest_articles_from_articles_list,
  remove_articles_list,
} from "../utils/articles_list";

interface Props {
  // articlesList
  articlesList: ArticlesList;
  // Update the articles to display
  updateArticles: (articleList: ArticlesList, articles: Article[]) => void;
  // Update articles lists
  getArticlesLists: () => void;
}

function ArticlesListItem(prop: Props) {
  function handleArticles() {
    get_latest_articles_from_articles_list(prop.articlesList).then(
      (jsonData) => {
        prop.updateArticles(
          prop.articlesList,
          jsonData["articles"] as unknown as Article[]
        );
      }
    );
  }

  function handleRemoveArticlesList() {
    remove_articles_list(prop.articlesList).then(() => {
      prop.getArticlesLists();
    });
  }

  return (
    <div className="accordion-item" id={String(prop.articlesList.id)}>
      <h2 className="accordion-header">
        <button
          className="accordion-button"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target={`#collapse_${prop.articlesList.id}`}
          aria-expanded="false"
          aria-controls={`collapse_${prop.articlesList.id}`}
          onClick={() => {
            handleArticles();
          }}
        >
          {prop.articlesList.name}
        </button>
      </h2>
      <div
        id={`collapse_${prop.articlesList.id}`}
        className="accordion-collapse collapse show"
        data-bs-parent="#accordionExample"
      >
        <div className="accordion-body">
          <button className="btn btn-light" onClick={handleRemoveArticlesList}>
            Remove
          </button>
        </div>
      </div>
    </div>
  );
}

export default ArticlesListItem;
