import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { login } from "../utils/auth";
import { DEBUG } from "../config";

function LoginForm() {
  let navigate = useNavigate();
  const [showAlert, setShowAlert] = useState(false);
  const [formData, setFormData] = useState({
    login: DEBUG ? "a" : "",
    password: DEBUG ? "a" : "",
  });

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const submitLogin = () => {
    login(formData)
      .then(() => {
        navigate("/main");
      })
      .catch(() => {
        setFormData({
          login: "",
          password: "",
        });
        setShowAlert(true);
      });
  };

  const register = () => {
    navigate("/register");
  };

  return (
    <>
      <div
        className="card my-3 mx-3 py-2 px-2 card border-0"
        style={{ width: "500px" }}
      >
        <div className="mb-3">
          <label className="form-label">Login</label>
          <input
            type="numeral"
            className="form-control"
            aria-describedby="emailHelp"
            name="login"
            value={formData.login}
            onChange={handleInputChange}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Password</label>
          <input
            type="password"
            className="form-control"
            id="exampleInputPassword1"
            name="password"
            value={formData.password}
            onChange={handleInputChange}
          />
        </div>
        <button onClick={submitLogin} className="btn btn-primary">
          Login
        </button>
        {showAlert && (
          <div className="mt-3">
            <p className="alert alert-danger text-center">
              Login failed ! Plesae check <u>login</u> or <u>password</u>.
            </p>
          </div>
        )}
      </div>
      <div
        className="card my-3 mx-3 py-2 px-2 card border-0"
        style={{ width: "250px", height: "100px" }}
      >
        <h3 className="text-center">New in the site ?</h3>
        <a className="btn btn-primary" onClick={register}>
          Register
        </a>
      </div>
    </>
  );
}

export default LoginForm;
