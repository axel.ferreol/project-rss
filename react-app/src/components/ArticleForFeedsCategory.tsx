import { Article, ArticlesList } from "../types/types";
import no_image from "../assets/no_image.webp";
import { useState } from "react";
import { add_article_to_articles_list } from "../utils/articles_list";
interface Props {
  article: Article;
  // Articles List of the user where the article can be added
  articlesLists: ArticlesList[];
}

function ArticleForFeedsCategory(prop: Props) {
  const [selectedArticlesList, setSelectedArticlesList] =
    useState<ArticlesList>({ id: -1, name: "unkown" });

  function handleAddArticletoArticlesList() {
    add_article_to_articles_list(selectedArticlesList, prop.article);
  }

  return (
    <>
      <div
        className="card justify-content-center mb-3 d-inline-block my-3 mx-3 py-2 px-2"
        style={{ width: "250px" }}
      >
        <img
          src={!prop.article.image_url ? no_image : prop.article.image_url}
          className="card-img-top"
          alt="..."
        />
        <div className="card-body">
          <h5 className="card-title"> {prop.article.title} </h5>
          <p className="card-text">
            {prop.article.description != null &&
              prop.article.description.slice(0, 200)}
          </p>
          <div className=" justify-content-center ">
            <a
              href={prop.article.url}
              target="_blank"
              className="btn btn-primary"
            >
              More
            </a>
          </div>
          <select
            className="form-select form-select-sm d-flex justify-content-center mt-4 mb-2"
            aria-label="Small select example"
            onChange={(event) => {
              setSelectedArticlesList({
                id: event.target.value as unknown as number,
                name: event.target.options[event.target.selectedIndex].text,
              });
            }}
          >
            <option disabled selected value={-1}>
              Add to articles list
            </option>
            {prop.articlesLists.map((articleList) => {
              return (
                <option value={articleList.id as unknown as number}>
                  {articleList.name}
                </option>
              );
            })}
          </select>
          <div className="d-flex justify-content-center">
            <button
              className="btn btn btn-outline-primary"
              onClick={handleAddArticletoArticlesList}
            >
              Add
            </button>
          </div>
        </div>
      </div>
    </>
  );
}
export default ArticleForFeedsCategory;
