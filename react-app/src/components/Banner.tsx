import { useNavigate } from "react-router-dom";

function Banner() {
  let navigate = useNavigate();
  const handleHeaderClick = () => {
    navigate("/main");
  };
  return (
    <div className="container">
      <h1
        className="display-4 text-center font-weight-bold text-success"
        onClick={handleHeaderClick}
      >
        RSS aggregator
      </h1>
    </div>
  );
}

export default Banner;
