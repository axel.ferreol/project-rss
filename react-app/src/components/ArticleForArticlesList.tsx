import { Article, ArticlesList } from "../types/types";
import no_image from "../assets/no_image.webp";
import {
  get_latest_articles_from_articles_list,
  remove_article_from_articles_list,
} from "../utils/articles_list";
interface Props {
  article: Article;
  articlesList: ArticlesList;
  // Update articles once it is removed
  setArticles: React.Dispatch<React.SetStateAction<Article[]>>;
}

function ArticleForArticlesList(prop: Props) {
  function handleRemoveArticleFromArticlesList() {
    remove_article_from_articles_list(prop.articlesList, prop.article).then(
      () => {
        get_latest_articles_from_articles_list(prop.articlesList).then(
          (jsonData) => {
            prop.setArticles(jsonData["articles"] as unknown as Article[]);
          }
        );
      }
    );
  }

  return (
    <>
      <div
        className="card  justify-content-center mb-3 d-inline-block my-3 mx-3 py-2 px-2"
        style={{ width: "250px" }}
      >
        <img
          src={!prop.article.image_url ? no_image : prop.article.image_url}
          className="card-img-top"
          alt="..."
        />
        <div className="card-body">
          <h5 className="card-title"> {prop.article.title} </h5>
          <p className="card-text">
            {prop.article.description != null &&
              prop.article.description.slice(0, 200)}
          </p>
          <div className=" justify-content-center ">
            <a
              href={prop.article.url}
              target="_blank"
              className="btn btn-primary"
            >
              More
            </a>
          </div>

          <div className="d-flex justify-content-center mt-3">
            <button
              className="btn btn-outline-primary"
              onClick={handleRemoveArticleFromArticlesList}
            >
              Remove
            </button>
          </div>
        </div>
      </div>
    </>
  );
}
export default ArticleForArticlesList;
