import { useNavigate } from "react-router-dom";
import { Article, FeedsCategory } from "../types/types";
import { get_latest_articles_from_feeds_category } from "../utils/feeds_category";

interface Props {
  // feedsCatgory
  feedsCatgory: FeedsCategory;
  // Update the articles to display
  setArticles: React.Dispatch<React.SetStateAction<Article[]>>;
}

function FeedsCategoryItem(prop: Props) {
  let navigate = useNavigate();
  function handleArticles() {
    get_latest_articles_from_feeds_category(prop.feedsCatgory).then(
      (jsonData) => {
        prop.setArticles(jsonData["articles"] as unknown as Article[]);
      }
    );
  }

  function handleManange() {
    navigate(`/feeds_categories/manage/${prop.feedsCatgory.id}`);
  }

  return (
    <div className="accordion-item" id={String(prop.feedsCatgory.id)}>
      <h2 className="accordion-header">
        <button
          className="accordion-button"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target={`#collapse_${prop.feedsCatgory.id}`}
          aria-expanded="false"
          aria-controls={`collapse_${prop.feedsCatgory.id}`}
          onClick={() => {
            handleArticles();
          }}
        >
          {prop.feedsCatgory.name}
        </button>
      </h2>
      <div
        id={`collapse_${prop.feedsCatgory.id}`}
        className="accordion-collapse"
        data-bs-parent="#accordionExample"
      >
        <div className="accordion-body">
          <button className="btn btn-light" onClick={handleManange}>
            Manage
          </button>
        </div>
      </div>
    </div>
  );
}

export default FeedsCategoryItem;
