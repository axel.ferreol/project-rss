import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { register } from "../utils/auth";
import { DEBUG } from "../config";

function RegisterForm() {
  let navigate = useNavigate();
  const [showAlert, setShowAlert] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string>("");
  const [formData, setFormData] = useState({
    login: "",
    password: "",
  });

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const submitRegister = () => {
    register(formData)
      .then(() => {
        navigate("/login");
      })
      .catch((error) => {
        setFormData({
          login: "",
          password: "",
        });
        setShowAlert(true);
        setErrorMessage(error);
      });
  };

  return (
    <>
      <div
        className="card my-3 mx-3 py-2 px-2 border-0"
        style={{ width: "500px" }}
      >
        <div className="mb-3">
          <label className="form-label">Login</label>
          <input
            type="numeral"
            className="form-control"
            aria-describedby="emailHelp"
            name="login"
            value={formData.login}
            onChange={handleInputChange}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Password</label>
          <input
            type="password"
            className="form-control"
            id="exampleInputPassword1"
            name="password"
            value={formData.password}
            onChange={handleInputChange}
          />
        </div>
        <button onClick={submitRegister} className="btn btn-primary">
          Register
        </button>
        <div className="mt-5">
          {showAlert && (
            <p className="alert alert-danger text-center">
              Register failed : {errorMessage}
            </p>
          )}
        </div>
      </div>
    </>
  );
}

export default RegisterForm;
