export type User = {
  login: string;
  password: string;
};

export type Feed = {
  id: number;
  url: string;
  name: string;
};

export type FeedsCategory = {
  id: number;
  name: string;
};

export type Article = {
  id: number;
  title: string;
  author: string;
  description: string;
  url: string;
  image_url: string;
  published_at: Date;
};

export type ArticlesList = {
  id: number;
  name: string;
};
