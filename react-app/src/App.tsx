import { createBrowserRouter, RouterProvider } from "react-router-dom";

import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Main from "./pages/Main";
import FeedsCategories from "./pages/FeedsCategories";
import ManagefeedsCategory from "./pages/ManageFeedsCategory";
import ArticlesLists from "./pages/ArticlesLists";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/main",
    element: <Main />,
  },
  {
    path: "/register",
    element: <Register />,
  },
  {
    path: "/feeds_categories",
    element: <FeedsCategories />,
  },
  {
    path: "/feeds_categories/manage/:feedCategoryId",
    element: <ManagefeedsCategory />,
  },
  {
    path: "/articles_lists",
    element: <ArticlesLists />,
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
