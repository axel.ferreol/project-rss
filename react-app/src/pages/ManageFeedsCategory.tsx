import { useNavigate, useParams } from "react-router-dom";
import { Feed, FeedsCategory } from "../types/types";
import Main from "./Main";
import { useEffect, useState } from "react";
import {
  add_feed,
  add_feed_to_feeds_category,
  get_feeds_from_feeds_category,
  get_list_feeds,
  remove_feed_from_feeds_category,
} from "../utils/feeds";
import { remove_feeds_category } from "../utils/feeds_category";

function ManagefeedsCategory() {
  let navigate = useNavigate();
  const feedsCategory: FeedsCategory = {
    id: useParams()["feedCategoryId"] as unknown as number,
    name: "unkown",
  };

  const [feeds, setFeeds] = useState<Feed[]>([]);
  const [feedsFromFeedsCategory, setFeedsFromFeedsCategory] = useState<Feed[]>(
    []
  );

  const getFeedsFromFeedsCategory = () => {
    get_feeds_from_feeds_category(feedsCategory).then((jsonData) => {
      setFeedsFromFeedsCategory(jsonData["feeds"] as Feed[]);
    });
  };
  const getFeeds = () => {
    get_list_feeds().then((jsonData) => {
      setFeeds(jsonData["feeds"] as Feed[]);
    });
  };

  function handleRemoveFeedsCategory() {
    remove_feeds_category(feedsCategory).then(() => {
      navigate("/feeds_categories");
    });
  }
  useEffect(() => {
    getFeedsFromFeedsCategory();
    getFeeds();
  }, []);
  return (
    <>
      <Main />
      <div className="text-center">
        <button className="btn btn-primary" onClick={handleRemoveFeedsCategory}>
          Delete Category
        </button>
      </div>
      <div>
        <h3>Add your feed</h3>
      </div>
      <div className="pr-2 pl-2">
        <NewFeed getFeeds={getFeeds} />
      </div>
      <div>
        <h3>My feeds</h3>
      </div>
      <div>
        {feedsFromFeedsCategory.map((feed) => (
          <MyFeed
            feed={feed}
            feedsCategory={feedsCategory}
            getFeedsFromFeedsCategory={getFeedsFromFeedsCategory}
          />
        ))}
      </div>
      <div>
        <h3>All feeds</h3>
      </div>
      <div>
        {feeds.map((feed) => (
          <AllFeed
            feed={feed}
            feedsCategory={feedsCategory}
            getFeedsFromFeedsCategory={getFeedsFromFeedsCategory}
          />
        ))}
      </div>
    </>
  );
}

// New feed
interface PropsNewFeed {
  // List feeds
  getFeeds: () => void;
}

function NewFeed(props: PropsNewFeed) {
  const [url, setUrl] = useState<string>("http://");
  const [nameFeed, setNameFeed] = useState<string>("My Feed");

  function handleOnChangeUrl(event: React.ChangeEvent<HTMLInputElement>) {
    setUrl(event.target.value);
  }

  function handleOnChangeNameFeed(event: React.ChangeEvent<HTMLInputElement>) {
    setNameFeed(event.target.value);
  }

  function handleButtonOnClickSubscribe() {
    const feed: Feed = { id: -1, url: url, name: nameFeed };
    add_feed(feed).then(() => {
      setNameFeed("My feed");
      setUrl("https://");
      props.getFeeds();
    });
  }

  return (
    <>
      <div
        className="card justify-content-center mb-3 d-inline-block my-3 mx-3 py-2 px-2"
        style={{ maxWidth: "250px" }}
      >
        <div className="card-body">
          <input
            value={nameFeed}
            onChange={handleOnChangeNameFeed}
            style={{ width: "200px" }}
          ></input>
          <input
            value={url}
            onChange={handleOnChangeUrl}
            style={{ width: "200px" }}
          ></input>
          <div className="d-flex justify-content-center mt-3">
            <button
              className="btn btn-primary"
              onClick={handleButtonOnClickSubscribe}
            >
              Add feed
            </button>
          </div>
        </div>
      </div>
    </>
  );
}
interface PropsMyFeed {
  // Feed to display
  feed: Feed;
  // FeedsCategory of the feed
  feedsCategory: FeedsCategory;
  // Update the feeds categories
  getFeedsFromFeedsCategory: () => void;
}

function MyFeed(prop: PropsMyFeed) {
  function handleRemoveFeedFromFeedsCategory() {
    remove_feed_from_feeds_category(prop.feedsCategory, prop.feed).then(() => {
      prop.getFeedsFromFeedsCategory();
    });
  }

  return (
    <div
      className="card justify-content-center mb-3 d-inline-block my-3 mx-3 py-2 px-2"
      style={{ maxWidth: "250px" }}
    >
      <div className="card-body">
        <h5 className="card-title d-flex justify-content-center mb-3">
          {prop.feed.name}
        </h5>
        <p className="card-text">
          {prop.feed.url != null && prop.feed.url.slice(0, 100)}
        </p>
        <div className="d-flex justify-content-center mt-3">
          <button
            className="btn btn-primary"
            onClick={handleRemoveFeedFromFeedsCategory}
          >
            Unsubscribe
          </button>
        </div>
      </div>
    </div>
  );
}

interface PropsAllFeed {
  // Feed
  feed: Feed;
  // Feeds category to add to
  feedsCategory: FeedsCategory;
  // Update the feeds categories
  getFeedsFromFeedsCategory: () => void;
}

function AllFeed(prop: PropsAllFeed) {
  function handleAddFeedToFeedsCategory() {
    add_feed_to_feeds_category(prop.feedsCategory, prop.feed).then(() => {
      prop.getFeedsFromFeedsCategory();
    });
  }

  return (
    <div
      className="card justify-content-center mb-3 d-inline-block my-3 mx-3 py-2 px-2"
      style={{ maxWidth: "250px" }}
    >
      <div className="card-body">
        <h5 className="card-title d-flex justify-content-center mb-3">
          {prop.feed.name}
        </h5>
        <p className="card-text">
          {prop.feed.url != null && prop.feed.url.slice(0, 100)}
        </p>
        <div className="d-flex justify-content-center mt-3">
          <button
            className="btn btn-primary"
            onClick={handleAddFeedToFeedsCategory}
          >
            Subscribe
          </button>
        </div>
      </div>
    </div>
  );
}
export default ManagefeedsCategory;
