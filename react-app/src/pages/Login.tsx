import BannerPreLogin from "../components/BannerPreLogin";
import LoginForm from "../components/LoginForm";

function Login() {
  return (
    <>
      <BannerPreLogin />
      <div className="d-flex justify-content-center mt-5">
        <LoginForm />
      </div>
    </>
  );
}

export default Login;
