import { useNavigate } from "react-router-dom";
import BannerPreLogin from "../components/BannerPreLogin";

function Home() {
  let navigate = useNavigate();

  function handleLogin() {
    navigate("/login");
  }

  function handleRegister() {
    navigate("/register");
  }

  return (
    <>
      <BannerPreLogin />
      <div className="mt-5">
        <h3 className="text-center">Welcome to the RSS aggregator</h3>
      </div>
      <div className="text-center mt-4">
        <button
          type="button"
          className="btn btn-primary btn-lg mx-2"
          onClick={handleLogin}
        >
          Login
        </button>
        <button
          type="button"
          className="btn btn-secondary btn-lg mx-2"
          onClick={handleRegister}
        >
          Register
        </button>
      </div>
    </>
  );
}
export default Home;
