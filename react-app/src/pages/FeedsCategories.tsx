import { useEffect, useState } from "react";
import { Article, ArticlesList, FeedsCategory } from "../types/types";
import {
  create_feeds_category,
  get_feeds_categories,
} from "../utils/feeds_category";
import Main from "./Main";
import FeedsCategoryItem from "../components/FeedsCategoryItem";
import ArticleForFeedsCategory from "../components/ArticleForFeedsCategory";
import { get_articles_lists } from "../utils/articles_list";

function FeedsCategories() {
  // The feeds categories of the user
  const [feedsCatgeories, setFeedsCateories] = useState<FeedsCategory[]>([]);
  const [dataFetched, setDataFetched] = useState(false);

  // Articles to display
  const [articles, setArticles] = useState<Article[]>([]);
  // Artciles Lists to where articles can be added
  const [articlesLists, setArticlesLists] = useState<ArticlesList[]>([]);

  function getFeedsCategories() {
    get_feeds_categories().then((jsonData) => {
      setFeedsCateories(
        jsonData["feeds_categories"] as unknown as FeedsCategory[]
      );
    });
  }
  function getArticlesLists() {
    get_articles_lists().then((jsonData) => {
      setArticlesLists(jsonData["articles_lists"] as unknown as ArticlesList[]);
    });
  }

  useEffect(() => {
    if (!dataFetched) {
      setDataFetched(true);
      getFeedsCategories();
      getArticlesLists();
    }
  }, [feedsCatgeories]);

  return (
    <>
      <Main />

      <div className="container-fluid vh-100 d-flex">
        <div className="col-md-4">
          <div className="overflow-auto">
            <div className="accordion" id="accordionExample">
              <NewFeedCategory getFeedsCategories={getFeedsCategories} />

              {feedsCatgeories.map((feedsCatgory) => {
                return (
                  <FeedsCategoryItem
                    feedsCatgory={feedsCatgory}
                    setArticles={setArticles}
                  />
                );
              })}
            </div>
          </div>
        </div>
        <div className="col-md-9">
          <div className="overflow-auto">
            <div>
              {articles.map((article) => {
                return (
                  <ArticleForFeedsCategory
                    article={article}
                    articlesLists={articlesLists}
                  />
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
interface Props {
  getFeedsCategories: () => void;
}
function NewFeedCategory(prop: Props) {
  const [newFeedsCategory, setNewFeedCategory] = useState<FeedsCategory>({
    id: -1,
    name: "New Feed Category",
  });

  function handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    const { name, value } = event.target;
    setNewFeedCategory({ ...newFeedsCategory, [name]: value });
  }
  function handleAddNewfeedsCategory() {
    create_feeds_category(newFeedsCategory).then(() => {
      prop.getFeedsCategories();
    });
  }
  return (
    <div className="accordion-item" id={String(-1)}>
      <h2 className="accordion-header">
        <button
          className="accordion-button"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#collapseOne"
          aria-expanded="true"
          aria-controls="collapseOne"
        >
          Add feeds category
        </button>
      </h2>
      <div
        id="collapseOne"
        className="accordion-collapse collapse show"
        data-bs-parent="#accordionExample"
      >
        <div className="accordion-body">
          <div className="row g-3 align-items-center">
            <div className="col-auto">
              <label className="col-form-label">Name</label>
            </div>
            <div className="col-auto">
              <input
                type="text"
                id="inputFeedCategory"
                name="name"
                className="form-control"
                aria-describedby="passwordHelpInline"
                value={newFeedsCategory.name} // Bind the input value to the state
                onChange={handleInputChange} // Handle input change event
              />
            </div>
            <div className="col-auto">
              <span id="passwordHelpInline" className="form-text">
                <button
                  className="btn btn-light"
                  onClick={handleAddNewfeedsCategory}
                >
                  Add
                </button>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FeedsCategories;
