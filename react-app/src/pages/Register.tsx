import BannerPreLogin from "../components/BannerPreLogin";
import RegisterForm from "../components/RegisterForm";
function Register() {
  return (
    <>
      <BannerPreLogin />
      <div className="d-flex justify-content-center mt-5">
        <RegisterForm />
      </div>
    </>
  );
}

export default Register;
