import { useEffect, useState } from "react";
import { Article, ArticlesList, FeedsCategory } from "../types/types";
import Main from "./Main";
import {
  create_articles_list,
  get_articles_lists,
} from "../utils/articles_list";
import ArticlesListItem from "../components/ArticlesListItem";
import ArticleForArticlesList from "../components/ArticleForArticlesList";

function ArticlesLists() {
  // The articles lists of the user
  const [articlesLists, setArticlesLists] = useState<ArticlesList[]>([]);
  const [dataFetched, setDataFetched] = useState(false);

  // Articles to display
  const [articles, setArticles] = useState<Article[]>([]);
  // ArticlesList associated to the articles
  const [articlesList, setArticlesList] = useState<ArticlesList>({
    id: -1,
    name: "unknown",
  });

  function updateArticles(articleList: ArticlesList, articles: Article[]) {
    setArticles(articles);
    setArticlesList(articleList);
  }
  function getArticlesLists() {
    get_articles_lists().then((jsonData) => {
      setArticlesLists(jsonData["articles_lists"] as unknown as ArticlesList[]);
    });
  }

  useEffect(() => {
    if (!dataFetched) {
      setDataFetched(true);
      getArticlesLists();
    }
  }, [articlesLists]);

  return (
    <>
      <Main />
      <div className="container-fluid vh-100 d-flex">
        <div className="col-md-4">
          <div className="overflow-auto">
            <div className="accordion" id="accordionExample">
              <NewArticlesList getArticlesLists={getArticlesLists} />
              {articlesLists.map((articlesList) => {
                return (
                  <ArticlesListItem
                    articlesList={articlesList}
                    updateArticles={updateArticles}
                    getArticlesLists={getArticlesLists}
                  />
                );
              })}
            </div>
          </div>
        </div>
        <div className="col-md-9">
          <div className="overflow-auto">
            <div>
              {articles.map((article) => {
                return (
                  <ArticleForArticlesList
                    article={article}
                    articlesList={articlesList}
                    setArticles={setArticles}
                  />
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
interface Props {
  // Update the articles lists
  getArticlesLists: () => void;
}
function NewArticlesList(prop: Props) {
  const [newArticlesList, setNewArticlesList] = useState<FeedsCategory>({
    id: -1,
    name: "New Articles List",
  });

  function handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    const { name, value } = event.target;
    setNewArticlesList({ ...newArticlesList, [name]: value });
  }
  function handleAddNewArticlesList() {
    create_articles_list(newArticlesList).then(() => {
      prop.getArticlesLists();
    });
  }
  return (
    <div className="accordion-item" id={String(-1)}>
      <h2 className="accordion-header">
        <button
          className="accordion-button"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#collapseOne"
          aria-expanded="true"
          aria-controls="collapseOne"
        >
          Add articles list
        </button>
      </h2>
      <div
        id="collapseOne"
        className="accordion-collapse collapse show"
        data-bs-parent="#accordionExample"
      >
        <div className="accordion-body">
          <div className="row g-3 align-items-center">
            <div className="col-auto">
              <label className="col-form-label">Name</label>
            </div>
            <div className="col-auto">
              <input
                type="text"
                id="inputFeedCategory"
                name="name"
                className="form-control"
                aria-describedby="passwordHelpInline"
                value={newArticlesList.name} // Bind the input value to the state
                onChange={handleInputChange} // Handle input change event
              />
            </div>
            <div className="col-auto">
              <span id="passwordHelpInline" className="form-text">
                <button
                  className="btn btn-light"
                  onClick={handleAddNewArticlesList}
                >
                  Add
                </button>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ArticlesLists;
