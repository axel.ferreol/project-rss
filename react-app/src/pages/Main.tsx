import { useNavigate } from "react-router-dom";
import { logout } from "../utils/auth";
import Banner from "../components/Banner";

function Main() {
  let navigate = useNavigate();

  function handleLogout() {
    logout()
      .then(() => {
        navigate("/");
      })
      .catch(() => {
        navigate("/");
      });
  }

  function handleFeeds() {
    navigate("/feeds_categories");
  }

  function handleArticlesLists() {
    navigate("/articles_lists");
  }

  return (
    <>
      <Banner />
      <ul className="nav justify-content-center">
        <li className="nav-item">
          <button
            className="nav-link"
            id="id_lists"
            onClick={handleArticlesLists}
          >
            Lists
          </button>
        </li>
        <li className="nav-item" id="id_feeds">
          <button className="nav-link" onClick={handleFeeds}>
            Feeds
          </button>
        </li>
        <li className="nav-item" id="id_logout">
          <button className="nav-link" onClick={handleLogout}>
            Logout
          </button>
        </li>
      </ul>
    </>
  );
}

export default Main;
