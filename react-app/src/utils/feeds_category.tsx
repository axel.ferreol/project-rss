import { host, port } from "../config";
import { FeedsCategory } from "../types/types";

export function get_feeds_categories() {
  const JWT = localStorage.getItem("access_token");
  return fetch(`http://${host}:${port}/feeds-category/get-feeds-categories`, {
    method: "GET",
    headers: { Authorization: "Bearer " + JWT },
  }).then((response) => {
    if (!response.ok) {
      throw new Error("Get feeds category failed");
    }
    return response.json();
  });
}

export function create_feeds_category(feeds_category: FeedsCategory) {
  const JWT = localStorage.getItem("access_token");
  return fetch(`http://${host}:${port}/feeds-category/create-feeds-category`, {
    method: "POST",
    headers: {
      Authorization: "Bearer " + JWT,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      feeds_category: feeds_category,
    }),
  }).then((response) => {
    if (!response.ok) {
      throw new Error("Create feeds category failed");
    }
    return response.json();
  });
}

export function remove_feeds_category(feeds_category: FeedsCategory) {
  const JWT = localStorage.getItem("access_token");
  return fetch(`http://${host}:${port}/feeds-category/remove-feeds-category`, {
    method: "POST",
    headers: {
      Authorization: "Bearer " + JWT,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      feeds_category: feeds_category,
    }),
  }).then((response) => {
    if (!response.ok) {
      throw new Error("Create feeds category failed");
    }
    return response.json();
  });
}

export function get_latest_articles() {
  const JWT = localStorage.getItem("access_token");
  return fetch(`http://${host}:${port}/feeds-category/get-latest-articles`, {
    method: "GET",
    headers: { Authorization: "Bearer " + JWT },
  }).then((response) => {
    if (!response.ok) {
      throw new Error("Create feeds category failed");
    }
    return response.json();
  });
}

export function get_latest_articles_from_feeds_category(
  feeds_category: FeedsCategory
) {
  const JWT = localStorage.getItem("access_token");
  return fetch(
    `http://${host}:${port}/feeds-category/get-latest-articles-from-feeds-category`,
    {
      method: "POST",
      headers: {
        Authorization: "Bearer " + JWT,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        feeds_category: feeds_category,
      }),
    }
  ).then((response) => {
    if (!response.ok) {
      throw new Error("Create feeds category failed");
    }
    return response.json();
  });
}
