import { host, port } from "../config";
import { Feed, FeedsCategory } from "../types/types";

export function get_list_feeds() {
  const JWT = localStorage.getItem("access_token");
  return fetch(`http://${host}:${port}/feeds/list-feeds`, {
    method: "GET",
    headers: { Authorization: "Bearer " + JWT },
  }).then((response) => {
    if (!response.ok) {
      throw new Error("List feeds failed");
    }
    return response.json();
  });
}

export function add_feed(feed: Feed) {
  const JWT = localStorage.getItem("access_token");
  return fetch(`http://${host}:${port}/feeds/add-feed`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + JWT,
    },
    body: JSON.stringify({
      feed: feed,
    }),
  }).then((response) => {
    if (!response.ok) {
      throw new Error("Subscribe to feeds failed");
    }
    return response.json();
  });
}

export function get_feeds_from_feeds_category(feeds_category: FeedsCategory) {
  const JWT = localStorage.getItem("access_token");
  return fetch(
    `http://${host}:${port}/feeds-category/get-feeds-from-feeds-category`,
    {
      method: "POST",
      headers: {
        Authorization: "Bearer " + JWT,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        feeds_category: feeds_category,
      }),
    }
  ).then((response) => {
    if (!response.ok) {
      throw new Error("Get feeds from feeds category failed");
    }
    return response.json();
  });
}

export function add_feed_to_feeds_category(
  feeds_category: FeedsCategory,
  feed: Feed
) {
  const JWT = localStorage.getItem("access_token");
  return fetch(
    `http://${host}:${port}/feeds-category/add-feed-to-feeds-category`,
    {
      method: "POST",
      headers: {
        Authorization: "Bearer " + JWT,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        feeds_category: feeds_category,
        feed: feed,
      }),
    }
  ).then((response) => {
    if (!response.ok) {
      throw new Error("Create feeds category failed");
    }
    return response.json();
  });
}

export function remove_feed_from_feeds_category(
  feeds_category: FeedsCategory,
  feed: Feed
) {
  const JWT = localStorage.getItem("access_token");
  return fetch(
    `http://${host}:${port}/feeds-category/remove-feed-from-feeds-category`,
    {
      method: "POST",
      headers: {
        Authorization: "Bearer " + JWT,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        feeds_category: feeds_category,
        feed: feed,
      }),
    }
  ).then((response) => {
    if (!response.ok) {
      throw new Error("Create feeds category failed");
    }
    return response.json();
  });
}
