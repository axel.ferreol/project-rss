import { DEBUG, host, port } from "../config";
import { User } from "../types/types";

export function login(user: User) {
  return fetch(`http://${host}:${port}/auth/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      username: user.login,
      password: user.password,
    }),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Login failed");
      }
      return response.json();
    })
    .then((jsonData) => {
      const JWT = jsonData["access_token"];
      localStorage.setItem("access_token", JWT);
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
}

export function logout() {
  const JWT = localStorage.getItem("access_token");
  return fetch(`http://${host}:${port}/auth/logout`, {
    method: "GET",
    headers: { Authorization: "Bearer " + JWT },
  }).then((response) => {
    if (!response.ok) {
      throw new Error("Error authentication logout");
    } else {
      localStorage.removeItem("access_token");
      return new Promise<void>((resolve) => {
        resolve();
      });
    }
  });
}

export function register(user: User) {
  return fetch(`http://${host}:${port}/auth/register`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      username: user.login,
      password: user.password,
    }),
  }).then((response) => {
    if (!response.ok) {
      return response.json().then((jsonData) => {
        return new Promise<string>((resolve, reject) => {
          reject(jsonData["message"]);
        });
      });
    }
    return response.json();
  });
}
