import { host, port } from "../config";
import { Article, ArticlesList } from "../types/types";

export function get_articles_lists() {
  const JWT = localStorage.getItem("access_token");
  return fetch(`http://${host}:${port}/articles-list/get-articles-lists`, {
    method: "GET",
    headers: {
      Authorization: "Bearer " + JWT,
    },
  }).then((response) => {
    if (!response.ok) {
      throw new Error("Get articles lists failed");
    }
    return response.json();
  });
}

export function create_articles_list(articles_list: ArticlesList) {
  const JWT = localStorage.getItem("access_token");
  return fetch(`http://${host}:${port}/articles-list/create-articles-list`, {
    method: "POST",
    headers: {
      Authorization: "Bearer " + JWT,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      articles_list: articles_list,
    }),
  }).then((response) => {
    if (!response.ok) {
      throw new Error("Create feeds category failed");
    }
    return response.json();
  });
}

export function remove_articles_list(articles_list: ArticlesList) {
  const JWT = localStorage.getItem("access_token");
  return fetch(`http://${host}:${port}/articles-list/remove-articles-list`, {
    method: "POST",
    headers: {
      Authorization: "Bearer " + JWT,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      articles_list: articles_list,
    }),
  }).then((response) => {
    if (!response.ok) {
      throw new Error("Create feeds category failed");
    }
    return response.json();
  });
}

export function add_article_to_articles_list(
  articles_list: ArticlesList,
  article: Article
) {
  const JWT = localStorage.getItem("access_token");
  return fetch(
    `http://${host}:${port}/articles-list/add-article-to-articles-list`,
    {
      method: "POST",
      headers: {
        Authorization: "Bearer " + JWT,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        articles_list: articles_list,
        article: article,
      }),
    }
  ).then((response) => {
    if (!response.ok) {
      throw new Error("Create feeds category failed");
    }
    return response.json();
  });
}

export function remove_article_from_articles_list(
  articles_list: ArticlesList,
  article: Article
) {
  const JWT = localStorage.getItem("access_token");
  return fetch(
    `http://${host}:${port}/articles-list/remove-article-from-articles-list`,
    {
      method: "POST",
      headers: {
        Authorization: "Bearer " + JWT,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        articles_list: articles_list,
        article: article,
      }),
    }
  ).then((response) => {
    if (!response.ok) {
      throw new Error("Create feeds category failed");
    }
    return response.json();
  });
}

export function get_latest_articles_from_articles_list(
  articles_list: ArticlesList
) {
  const JWT = localStorage.getItem("access_token");
  return fetch(
    `http://${host}:${port}/articles-list/get-latest-articles-from-articles-list`,
    {
      method: "POST",
      headers: {
        Authorization: "Bearer " + JWT,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        articles_list: articles_list,
      }),
    }
  ).then((response) => {
    if (!response.ok) {
      throw new Error("Create feeds category failed");
    }
    return response.json();
  });
}
